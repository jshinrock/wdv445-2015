<div class="site-container">
    
    <div class="gradient-overlay">
    </div>
    
    <!-- header -->
    <div class="header-wrapper">
    </div>
    
    <!-- site title -->
    <div class="site-title-container">
        <div class="site-title">
            <p>Photography Blog</p>
        </div>
    </div>
    
    <!-- content -->
    <div class="content-container container">
        <div class="menu-container inner-container">
            <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
        </div>
        
        <!-- tabs will go here -->
        <div class="tab-container container">
            <?php if ($tabs): ?>
                  <?php print render($tabs); ?>
            <?php endif; ?>
        </div>
        
        <div class="content inner-container clearfix">
            <div class="main-content <?php print $variables["photo_blog"]["main-content-width"]; ?> left">
                <div class="content">
                    
                    <!-- messages will go here -->
                    <?php if ($messages): ?>
                        <div id="messages">
                            <div class="section clearfix">
                                <?php print $messages; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                  <?php print render($page['content']); ?>
                </div>
            </div>
              <?php if($page['right_column']): ?>
                <div class="right-column column region one-fourth left">
                    <div class="right-column-inner">
                        <?php print render($page['right_column']); ?>
                    </div>
                </div>
              <?php endif; ?>
        </div>
    </div>
    
    <!-- footer -->
    <div class="footer-container container">
        <?php if($page['footer']): ?>
            <div class="footer-content inner-container">
                <?php print render($page['footer']); ?>
            </div>
        <?php endif; ?>
    </div>
    
</div>
