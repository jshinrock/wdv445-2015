<?php
  
  function photo_blog_preprocess_page(&$variables){
    
    $page = $variables["page"];
    $variables["photo_blog"]["main-content-width"] = "three-fourths";
    
    if(empty($page["right_column"])){
      // we need to assign a class "full-width" to main content
      
      $variables["photo_blog"]["main-content-width"] = "full-width";
      
    }
    
  }
