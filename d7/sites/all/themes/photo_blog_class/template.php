<?php
  
function photo_blog_class_preprocess_page(&$variables){
  
  $page = $variables['page'];
  $content_class = "";
    
  if(empty($page['left_column'])){
    $content_class = "no-left-padding";
  }
  
  $variables['photo_blog_class']['content_class'] = $content_class;
}
